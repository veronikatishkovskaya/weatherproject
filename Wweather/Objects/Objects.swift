import Foundation

class Objects: Codable {
    var lat: Double?
    var lon: Double?
    var timezone: String?
    var timezone_offset: Int?
    var current: CurrentWeather?
    var daily: [DailyWeather]?
    var hourly: [HourlyWeather]?
}

class CurrentWeather: Codable {
    var dt: Int?
    var sunrise: Int?
    var sunset: Int?
    var temp: Double?
    var feels_like: Double?
    var pressure: Int?
    var humidity: Int?
    var wind_speed: Double?
    var weather: [Weather]?
}

class Weather: Codable {
    var main: String?
    var description: String?
}

class DailyWeather: Codable {
    var dt: Int?
    var temp: Temperature?
    var weather: [DailyWeatherCondition]?
}

class Temperature: Codable {
    var day: Double?
    var night: Double?
}

class DailyWeatherCondition: Codable {
    var main: String?
    var description: String?
}

class HourlyWeather: Codable {
    var dt: Int?
    var temp: Double?
    var weather: [HourlyWeatherCondition]?
}

class HourlyWeatherCondition: Codable {
    var main: String?
    var description: String?
}
