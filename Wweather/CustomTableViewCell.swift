import UIKit

class CustomTableViewCell: UITableViewCell {
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dayTemperatureLabel: UILabel!
    @IBOutlet weak var nightTemperatureLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    
    let timezone = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(object: DailyWeather) {
        guard let date = object.dt else {return}
        self.dayLabel.text = getDayForDate(Date(timeIntervalSince1970: Double(date)))
        let temperature = object.temp
        self.dayTemperatureLabel.text = "▵ \(Int(temperature?.day ?? 0))°"
        self.nightTemperatureLabel.text = "▿ \(Int(temperature?.night ?? 0))°"
        
        guard let weathers = object.weather else {return}
        for weather in weathers {
            let mainWeather = weather.main
            let weather = weather.description
            
            
            if mainWeather == "Clear" {
                self.weatherIcon.image = UIImage(named: "sunnyImage")
            } else if mainWeather == "Mist" {
                self.weatherIcon.image = UIImage(named: "mistImage")
            } else if mainWeather == "Clouds" {
                self.weatherIcon.image = UIImage(named: "cloudyImage")
            } else if weather == "few clouds" {
                self.weatherIcon.image = UIImage(named: "cloudySunImage")
            } else if mainWeather == "Rain" {
                self.weatherIcon.image = UIImage(named: "heavyRainImage")
            } else if weather == "light rain" {
                self.weatherIcon.image = UIImage(named: "rainyImage")
            } else if mainWeather == "Thunderstorm" {
                self.weatherIcon.image = UIImage(named: "thunderstormImage")
            } else if mainWeather == "Snow" {
                self.weatherIcon.image = UIImage(named: "snowImage")
            }
        }
    }
    
    
    
}
extension CustomTableViewCell {
    func getDayForDate(_ date: Date?) -> String {
        guard let inputDate = date else {
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE"
        return formatter.string(from: inputDate)
    }
}
extension CustomTableViewCell {
    
    func convertDatatoLocalTimezone(date: Int, timezone: Int) -> Int {
        let currentTimezone = TimeZone.current.secondsFromGMT()
        let convertedTime = date - (currentTimezone - timezone)
        return convertedTime
    }
}

