import Foundation
import UIKit
import CoreLocation

class ViewModel: NSObject, CLLocationManagerDelegate {
    
    var weatherInfo = Bindable<Objects?>(nil)
    var temperature = Bindable<Double?>(nil)
    var date = Bindable<Int?>(nil)
    var timezone = Bindable<Int?>(nil)
    var condition = Bindable<String?>(nil)
    var feelsLike = Bindable<Double?>(nil)
    var sunrise = Bindable<Int?>(nil)
    var sunset = Bindable<Int?>(nil)
    var humidity = Bindable<Int?>(nil)
    var pressure = Bindable<Int?>(nil)
    var city = Bindable<String?>(nil)
    var currentLocation: CLLocation?
    var location: CLLocationCoordinate2D?
    let locationManager = CLLocationManager()
    var locationLatitude = Double()
    var locationLongtitude = Double()
    var defaultCityLoaded = false
    
    override init() {
        super.init()
    }
    
    func getCurrentWeather() {
        getCoordinateFrom(address: self.city.value!) { (coordinate, error) in
            guard let coordinate = coordinate, error == nil else { return }
            self.locationLatitude = coordinate.latitude
            self.locationLongtitude = coordinate.longitude
        }
        NetworkManager.shared.getWeather(lat: self.locationLatitude, lon: self.locationLongtitude) { (result) in
            self.weatherInfo.value = result
            guard let timezone = result?.timezone_offset else {return}
            self.timezone.value = timezone
            guard let current = result?.current else {return}
            self.date.value = current.dt
            self.sunrise.value = current.sunrise
            self.sunset.value = current.sunset
            self.temperature.value = current.temp
            self.feelsLike.value = current.feels_like
            self.pressure.value = current.pressure
            self.humidity.value = current.humidity
            
            guard let weatherArray = current.weather else {return}
            for weather in weatherArray {
                self.condition.value = weather.description
            }  
        }
    }
    
    func getWeatherByCityName(lat: Double, lon: Double) {
        NetworkManager.shared.getWeather(lat: lat, lon: lon) { (result) in
            self.weatherInfo.value = result
            guard let timezone = result?.timezone_offset else {return}
            self.timezone.value = timezone
            guard let current = result?.current else {return}
            self.date.value = current.dt
            self.sunrise.value = current.sunrise
            self.sunset.value = current.sunset
            self.temperature.value = current.temp
            self.feelsLike.value = current.feels_like
            self.pressure.value = current.pressure
            self.humidity.value = current.humidity
            
            guard let weatherArray = current.weather else {return}
            for weather in weatherArray {
                self.condition.value = weather.description
            }
        }
    }
    
    func getLocation() {
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        DispatchQueue.main.async {
            let location: CLLocationCoordinate2D = manager.location!.coordinate
            self.locationLatitude = location.latitude
            self.locationLongtitude = location.longitude
            
            if self.defaultCityLoaded == false {
                let newLocation = locations.last
                self.currentLocation = newLocation
                let geocoder = CLGeocoder()
                geocoder.reverseGeocodeLocation(newLocation!) { (placemarks, error) in
                    if let error = error {
                        debugPrint(error.localizedDescription)
                    }
                    if let placemarks = placemarks {
                        if placemarks.count > 0 {
                            let placemarks = placemarks[0]
                            if let city = placemarks.locality {
                                self.city.value = city.localized
                                self.defaultCityLoaded = true
                                self.getCurrentWeather()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func getCoordinateFrom(address: String, completion: @escaping(_ coordinate: CLLocationCoordinate2D?, _ error: Error?) -> () ) {
        CLGeocoder().geocodeAddressString(address) { placemarks, error in
            completion(placemarks?.first?.location?.coordinate, error)
        }
    }
    
}
extension CLLocation {
    func fetchCityAndCountry(completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(self) { completion($0?.first?.locality, $0?.first?.country, $1) }
    }
}

