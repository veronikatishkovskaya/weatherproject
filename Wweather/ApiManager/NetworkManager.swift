import Foundation
import CoreLocation

class NetworkManager {
    
    static let shared = NetworkManager()
    
    private init() {}
    
    func getWeather(lat: CLLocationDegrees, lon: CLLocationDegrees, completion: @escaping ((Objects?) -> ())) {
        var objects: Objects?
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "api.openweathermap.org"
        urlComponents.path = "/data/2.5/onecall"
        urlComponents.queryItems = [URLQueryItem(name: "lat", value: "\(lat)"),
                                    URLQueryItem(name: "lon", value: "\(lon)"),
                                    URLQueryItem(name: "appid", value: "07d4838b3f6736d2e67abd4f608a88ac"),
                                    URLQueryItem(name: "units", value: "metric"),
                                //  URLQueryItem(name: "lang", value: "ru")
                ]
        
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data {
                do {
                    objects = try JSONDecoder().decode(Objects.self, from: data)
                    completion(objects)
                } catch {
                    print(error)
                }
            }
        }
        task.resume()
    }
    
}
