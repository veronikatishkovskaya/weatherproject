import UIKit
import CoreLocation
import WebKit

class ViewController: UIViewController, CLLocationManagerDelegate {
    //MARK: - IBOutlets
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var searchBarView: UIView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var conditionLabel: UILabel!
    @IBOutlet weak var sunriseTime: UILabel!
    @IBOutlet weak var sunsetTime: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var feelslikeTemperatureLable: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var searchCityTextField: UITextField!
    @IBOutlet weak var weekForecastView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var backgrounImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    //MARK: - let
    let locationManager = CLLocationManager()
    //MARK: - var
    var currentWeatherArray: [CurrentWeather] = []
    var dailyWeatherArray: [DailyWeather] = []
    var hourlyWeatherArray: [HourlyWeather] = []
    var latitude: Double?
    var longtitude: Double?
    var city: String?
    var viewModel: ViewModel?
    var timezone = 0
    var currentTime = 0
    var sunset = 0
    var sunrise = 0
    //MARK: - VC func flow
    override func viewDidLoad() {
        super.viewDidLoad()
        setHourForecastViewSetting()
        searchBarSetting()
        self.viewModel = ViewModel()
        viewModel?.getLocation()
        getLocalWeather()
        getCity()
        registerForKeyboardNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchCityTextField.delegate = self
        collectionView.delegate = self
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
    }
    
    //MARK: - IBAction
    @IBAction private func keyboardWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo,
              let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
              let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
    }
    
    @IBAction func goToWeekWeatherForecast(_ sender: UIButton) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "DailyWeatherViewController") as! DailyWeatherViewController
        controller.dailyWeatherArray = self.dailyWeatherArray
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK: - func flow
    
    func getCity() {
        DispatchQueue.main.async {
            self.viewModel?.city.bind({ (city) in
                guard let city = city else {return}
                self.cityNameLabel.text = city
            })
        }
    }
    
    func getLocalWeather() {
        viewModel?.weatherInfo.bind({ (weather) in
            DispatchQueue.main.async {
                self.startLoading()
                guard let elements = weather else {return}
                if let currentWeather = elements.current {
                    self.currentWeatherArray.append(currentWeather)
                }
                if let hourWeather = elements.hourly {
                    self.hourlyWeatherArray = hourWeather
                    self.collectionView.reloadData()
                }
                if let dayWeather = elements.daily {
                    self.dailyWeatherArray = dayWeather
                }
            }
        })
        
        self.viewModel?.timezone.bind({ (timezone) in
            DispatchQueue.main.async {
                guard let timezone = timezone else {return}
                self.timezone = timezone
            }
        })
        viewModel?.sunrise.bind({ (sunrise) in
            DispatchQueue.main.async {
                guard let sunrise = sunrise else {return}
                let timeSunrise = self.convertDatatoLocalTimezone(date: sunrise, timezone: self.timezone)
                let sunriseTime = self.getDayForDate(Date(timeIntervalSince1970: Double(timeSunrise)))
                self.sunrise = timeSunrise
                self.sunriseTime.text = "\(sunriseTime)"
            }
        })
        viewModel?.sunset.bind({ (sunset) in
            DispatchQueue.main.async {
                guard let sunset = sunset else {return}
                let timeSunset = self.convertDatatoLocalTimezone(date: sunset, timezone: self.timezone)
                let sunsetTime = self.getDayForDate(Date(timeIntervalSince1970: Double(timeSunset)))
                self.sunset = timeSunset
                self.sunsetTime.text = "\(sunsetTime)"
            }
        })
        viewModel?.date.bind({ (date) in
            DispatchQueue.main.async {
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateStyle = .full
                let timeInterval = date.timeIntervalSince1970
                let convertedDate = Int(timeInterval)
                let timezoneDate = self.convertDatatoLocalTimezone(date: convertedDate, timezone: self.timezone)
                let currentDate = Date(timeIntervalSince1970: Double(timezoneDate))
                self.currentTime = timezoneDate
                self.dateLabel.text = formatter.string(from: currentDate)
            }
        })
        viewModel?.temperature.bind({ (temperature) in
            DispatchQueue.main.async {
                guard let temperature = temperature else {return}
                self.temperatureLabel.text = "\(Int(temperature))°"
            }
        })
        
        viewModel?.condition.bind({ (condition) in
            DispatchQueue.main.async {
                guard let condition = condition else {return}
                self.conditionLabel.text = condition
            }
        })
        
        viewModel?.feelsLike.bind({ (feelsLike) in
            DispatchQueue.main.async {
                guard let feelsLike = feelsLike else {return}
                self.feelslikeTemperatureLable.text = "feels like \(Int(feelsLike))°"
            }
        })
        
        viewModel?.humidity.bind({ (humidity) in
            DispatchQueue.main.async {
                guard let humidity = humidity else {return}
                self.humidityLabel.text = "humidity \(humidity)%"
            }
        })
        viewModel?.pressure.bind({ (pressure) in
            DispatchQueue.main.async {
                guard let pressure = pressure else {return}
                self.pressureLabel.text = "pressure \(pressure)hPa"
                
                if self.currentTime < self.sunset && self.currentTime > self.sunrise {
                    self.setDayImages()
                } else {
                    self.setNightImages()
                }
                
                self.finishLoading()
            }
        })
        
    }
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func searchBarSetting() {
        searchBarView.cornerRadius(15)
        searchBarView.backgroundColor = .clear
        searchBarView.layer.borderWidth = 2
        searchBarView.layer.borderColor = CGColor(red: 255, green: 255, blue: 255, alpha: 1)
    }
    
    func setHourForecastViewSetting() {
        weekForecastView.cornerRadius(45)
    }
    
    func setDayImages() {
        DispatchQueue.main.async {
            let weathers = self.currentWeatherArray
            for weather in weathers {
                let conditions = weather.weather!
                for condition in conditions {
                    let mainWeather = condition.main
                    let weather = condition.description
                    
                    if mainWeather == "Clear" {
                        self.weatherIcon.image = UIImage(named: "sunnyImage")
                        self.backgrounImageView.image = UIImage(named: "dayBackImage")
                        self.backgrounImageView.contentMode = .scaleAspectFill
                        
                    } else if mainWeather == "Mist" {
                        self.weatherIcon.image = UIImage(named: "MainMistImage")
                        
                        self.backgrounImageView.image = UIImage(named: "cloudsBackImage")
                        self.backgrounImageView.contentMode = .scaleAspectFill
                        
                    } else if mainWeather == "Clouds" {
                        self.weatherIcon.image = UIImage(named: "MainCloudImage")
                        
                        self.backgrounImageView.image = UIImage(named: "cloudsBackImage")
                        self.backgrounImageView.contentMode = .scaleAspectFill
                        
                    } else if weather == "few clouds" {
                        self.weatherIcon.image = UIImage(named: "cloudySunImage")
                        
                        self.backgrounImageView.image = UIImage(named: "dayBackImage")
                        self.backgrounImageView.contentMode = .scaleAspectFill
                        
                    } else if mainWeather == "Rain" {
                        self.weatherIcon.image = UIImage(named: "MainRainImage")
                        
                        self.backgrounImageView.image = UIImage(named: "rainBackImage")
                        self.backgrounImageView.contentMode = .scaleAspectFill
                        
                    } else if weather == "light rain" {
                        self.weatherIcon.image = UIImage(named: "MainLightRainImage")
                        
                        self.backgrounImageView.image = UIImage(named: "rainBackImage")
                        self.backgrounImageView.contentMode = .scaleAspectFill
                        
                    } else if mainWeather == "Thunderstorm" {
                        self.weatherIcon.image = UIImage(named: "MainThunderImage")
                        
                        self.backgrounImageView.image = UIImage(named: "rainBackImage")
                        self.backgrounImageView.contentMode = .scaleAspectFill
                        
                    } else if mainWeather == "Snow" {
                        self.weatherIcon.image = UIImage(named: "MainSnowImage")
                        
                        self.backgrounImageView.image = UIImage(named: "cloudsBackImage")
                        self.backgrounImageView.contentMode = .scaleAspectFill
                        
                    }
                }
            }
        }
        
    }
    func setNightImages() {
        DispatchQueue.main.async {
            let weathers = self.currentWeatherArray
            for weather in weathers {
                let conditions = weather.weather!
                for condition in conditions {
                    let mainWeather = condition.main
                    let weather = condition.description
                    
                    if mainWeather == "Clear" {
                        self.weatherIcon.image = UIImage(named: "clearNight")
                        self.backgrounImageView.image = UIImage(named: "nightBackImage")
                        self.backgrounImageView.contentMode = .scaleAspectFill
                        
                    } else if mainWeather == "Mist" {
                        self.weatherIcon.image = UIImage(named: "MainMistImage")
                        
                        self.backgrounImageView.image = UIImage(named: "cloudsBackImage")
                        self.backgrounImageView.contentMode = .scaleAspectFill
                        
                    } else if mainWeather == "Clouds" {
                        self.weatherIcon.image = UIImage(named: "MainCloudImage")
                        
                        self.backgrounImageView.image = UIImage(named: "cloudsBackImage")
                        self.backgrounImageView.contentMode = .scaleAspectFill
                        
                    } else if weather == "few clouds" {
                        self.weatherIcon.image = UIImage(named: "MainNightCloudImage")
                        
                        self.backgrounImageView.image = UIImage(named: "nightBackImage")
                        self.backgrounImageView.contentMode = .scaleAspectFill
                        
                    } else if mainWeather == "Rain" {
                        self.weatherIcon.image = UIImage(named: "MainRainImage")
                        
                        self.backgrounImageView.image = UIImage(named: "rainBackImage")
                        self.backgrounImageView.contentMode = .scaleAspectFill
                        
                    } else if weather == "light rain" {
                        self.weatherIcon.image = UIImage(named: "MainLightRainImage")
                        
                        self.backgrounImageView.image = UIImage(named: "rainBackImage")
                        self.backgrounImageView.contentMode = .scaleAspectFill
                        
                    } else if mainWeather == "Thunderstorm" {
                        self.weatherIcon.image = UIImage(named: "MainThunderImage")
                        
                        self.backgrounImageView.image = UIImage(named: "rainBackImage")
                        self.backgrounImageView.contentMode = .scaleAspectFill
                        
                    } else if mainWeather == "Snow" {
                        self.weatherIcon.image = UIImage(named: "MainSnowImage")
                        
                        self.backgrounImageView.image = UIImage(named: "cloudsBackImage")
                        self.backgrounImageView.contentMode = .scaleAspectFill
                        
                    }
                }
            }
        }
        
    }
    
    func startLoading() {
        DispatchQueue.main.async {
            self.blurView.isHidden = false
            self.activityIndicator.startAnimating()
            self.view.addSubview(self.activityIndicator)
        }
    }
    
    func finishLoading() {
        DispatchQueue.main.async {
            self.blurView.isHidden = true
            self.activityIndicator.stopAnimating()
            self.activityIndicator.hidesWhenStopped = true
        }
    }
    
    
    
    
    
    
}
//MARK: - Extensions
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hourlyWeatherArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell", for: indexPath) as? CustomCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        cell.timezone = self.timezone
        
        cell.configure(object: hourlyWeatherArray[indexPath.item])
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side: CGFloat = collectionView.frame.size.width
        return CGSize(width: side, height: side)
    }
}
extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}

extension ViewController: UITextFieldDelegate  {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        startLoading()
        self.city = self.searchCityTextField.text!
        self.cityNameLabel.text = self.city
        self.viewModel?.getCoordinateFrom(address: self.city!, completion: { (coordinate, error) in
            self.latitude = coordinate?.latitude
            self.longtitude = coordinate?.longitude
            self.viewModel?.getWeatherByCityName(lat: self.latitude!, lon: self.longtitude!)
            self.getLocalWeather()
        })
        searchCityTextField.resignFirstResponder()
        
        return true
    }
}

extension UIView {
    func cornerRadius(_ radius: CGFloat) {
        self.layer.cornerRadius = radius
    }
}


extension ViewController {
    func getDayForDate(_ date: Date?) -> String {
        guard let inputDate = date else {
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: inputDate)
    }
}
extension ViewController {
    
    func convertDatatoLocalTimezone(date: Int, timezone: Int) -> Int {
        let currentTimezone = TimeZone.current.secondsFromGMT()
        let convertedTime = date - (currentTimezone - timezone)
        return convertedTime
    }
}
