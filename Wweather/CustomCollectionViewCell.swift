import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var hourlyWeatherView: UIView!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    
    var timezone = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(object: HourlyWeather) {
        guard let date = object.dt else {return}
        let time = self.convertDatatoLocalTimezone(date: date, timezone: self.timezone )
        let convertedTime = self.getDayForDate(Date(timeIntervalSince1970: Double(time)))
        self.hourLabel.text = convertedTime
        self.temperatureLabel.text = "\(Int(object.temp ?? 0))°"
        
        let weathers = object.weather!
        for weather in weathers {
            let mainWeather = weather.main
            let weather = weather.description
            
            if mainWeather == "Clear" {
                self.weatherIcon.image = UIImage(named: "sunnyImage")
            } else if mainWeather == "Mist" {
                self.weatherIcon.image = UIImage(named: "mistImage")
            } else if mainWeather == "Clouds" {
                self.weatherIcon.image = UIImage(named: "cloudyImage")
            } else if weather == "few clouds" {
                self.weatherIcon.image = UIImage(named: "cloudySunImage")
            } else if mainWeather == "Rain" {
                self.weatherIcon.image = UIImage(named: "heavyRainImage")
            } else if weather == "light rain" {
                self.weatherIcon.image = UIImage(named: "rainyImage")
            } else if mainWeather == "Thunderstorm" {
                self.weatherIcon.image = UIImage(named: "thunderstormImage")
            } else if mainWeather == "Snow" {
                self.weatherIcon.image = UIImage(named: "snowImage")
            }
        }
        
        viewSettings()
    }
    
    func viewSettings() {
        self.hourlyWeatherView.layer.borderWidth = 2
        self.hourlyWeatherView.layer.borderColor = CGColor(red: 0, green: 0, blue: 0, alpha: 0.2)
        self.hourlyWeatherView.layer.cornerRadius = 15
    }
}

extension CustomCollectionViewCell {
    func getDayForDate(_ date: Date?) -> String {
        guard let inputDate = date else {
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: inputDate)
    }
}
extension CustomCollectionViewCell {
    
    func convertDatatoLocalTimezone(date: Int, timezone: Int) -> Int {
        let currentTimezone = TimeZone.current.secondsFromGMT()
        let convertedTime = date - (currentTimezone - timezone)
        return convertedTime
    }
}

