import UIKit

class DailyWeatherViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tomorrowForecastView: UIView!
    @IBOutlet weak var dayTemperatureLabel: UILabel!
    @IBOutlet weak var nightTemperatureLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    
    var dailyWeatherArray: [DailyWeather] = []
    var object = DailyWeather()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tomorrowForecastView.cornerRadius(15)
        setTomorrowWeather()
        tableView.reloadData()
        
    }
    @IBAction func goBackToCurrentWeather(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setTomorrowWeather() {
        let weather = self.dailyWeatherArray[1]
        let tomorrowWeather = weather.temp
        let dayTemperature = tomorrowWeather?.day
        let nightTemperature = tomorrowWeather?.night
        self.dayTemperatureLabel.text = "▵\(Int(dayTemperature ?? 0))°"
        self.nightTemperatureLabel.text = "▿\(Int(nightTemperature ?? 0))°"
        guard let currentTime = weather.dt else {return}
        self.dayLabel.text = getDayForDate(Date(timeIntervalSince1970: Double(currentTime)))
        
        let weathers = dailyWeatherArray
        for weather in weathers {
            guard let condition = weather.weather else {return}
            for weatherCondition in condition {
                let mainWeather = weatherCondition.main
                let weather = weatherCondition.description
                
                if mainWeather == "Clear" {
                    self.weatherIcon.image = UIImage(named: "sunnyImage")
                } else if mainWeather == "Mist" {
                    self.weatherIcon.image = UIImage(named: "mistImage")
                } else if mainWeather == "Clouds" {
                    self.weatherIcon.image = UIImage(named: "cloudyImage")
                } else if weather == "few clouds" {
                    self.weatherIcon.image = UIImage(named: "cloudySunImage")
                } else if mainWeather == "Rain" {
                    self.weatherIcon.image = UIImage(named: "heavyRainImage")
                } else if weather == "light rain" {
                    self.weatherIcon.image = UIImage(named: "rainyImage")
                } else if mainWeather == "Thunderstorm" {
                    self.weatherIcon.image = UIImage(named: "thunderstormImage")
                } else if mainWeather == "Snow" {
                    self.weatherIcon.image = UIImage(named: "snowImage")
                }
            }
        }
    }
}
extension DailyWeatherViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dailyWeatherArray.count - 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as? CustomTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(object: dailyWeatherArray[indexPath.row + 2])
        return cell
    }
    
    
}
extension DailyWeatherViewController {
    func getDayForDate(_ date: Date?) -> String {
        guard let inputDate = date else {
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE"
        return formatter.string(from: inputDate)
    }
}

